// variables y constantes

const vector = ["Lunes", "martes", "miercoles", "jueves", "viernes"];

//entradas
vector[10] = "Sabado";

// recorrer el array y mostrarle en pantalla

// utilizando for
for (let i = 0; i < vector.length; i++) {
    document.write(vector[i] + "<br>");
}
// utilizando foreach
vector.forEach((dia) => {
    document.write(dia + "<br>");
});

// utilizando for of

for (const dia of vector) {
    document.write(dia + "<br>");
}

// utilizando for of (leyendo indice y valor)

for (let [indice, valor] of vector.entries()) {
    document.write(indice, valor + "<br>");
}

// utilizando for in

for (const i in vector) {
    document.write(vector[i] + "<br>");
}

// utilizando while

let i = 0;
while (i < vector.length) {
    document.write(vector[i] + "<br>");
    i++;
}
