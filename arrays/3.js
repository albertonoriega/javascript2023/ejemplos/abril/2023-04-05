// variables y constantes
const li = document.querySelectorAll('ul>li');

// Cuando se pulsa un li que cambie el color de fondo a rojo 

// utilizando foreach
li.forEach(function (valor) {
    valor.addEventListener('click', function (evento) {
        evento.target.style.backgroundColor = "red";
    });
});

// // En vez de colocar un escuchador a cada li, coloco un escuchador al ul

// const ul = document.querySelector('ul');

// ul.addEventListener('click', function (evento) {
//     evento.target.style.backgroundColor = "red";

// });