let botonEncender = document.querySelector('#encender');
let botonApagar = document.querySelector('#apagar');
let botonEliminar = document.querySelector('#eliminar');

botonEncender.addEventListener('click', () => {
    document.querySelector("img").style.display = 'inline-block';
    document.querySelector("img").src = "img/on.gif";
    document.querySelector("img").alt = "bombilla encendida";
});

botonApagar.addEventListener('click', () => {
    document.querySelector("img").style.display = 'inline-block';
    document.querySelector("img").src = "img/off.gif";
    document.querySelector("img").alt = "bombilla apagada";
});

botonEliminar.addEventListener('click', () => {
    document.querySelector("img").style.display = 'none';
});