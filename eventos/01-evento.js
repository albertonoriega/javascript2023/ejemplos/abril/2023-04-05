// variable que apunta a los elementos que quiero
// colocar el escuchador (onclick)
const cajas = document.querySelectorAll("div");

// recorro cada uno de los elementos del array 
// y les coloco el listener (onclick)
// for (let caja of cajas) {
//     caja.addEventListener("click", function (e) {
//         // e es el event que desencadena el evento

//         //cambio el fondo del div sobre el que se pulsa
//         e.target.style.backgroundColor = "red";
//         //cambio el texto del div sobre el que se pulsa
//         e.target.innerHTML = "";
//     });
// }


// realizarlo con foreach

cajas.forEach(function (valor) {
    valor.addEventListener('click', function (evento) {
        //cambio el fondo del div sobre el que se pulsa
        evento.target.style.backgroundColor = "red";
        //cambio el texto del div sobre el que se pulsa
        evento.target.innerHTML = "";
    })
})
