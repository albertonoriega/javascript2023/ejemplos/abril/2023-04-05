const cajas = document.querySelectorAll("div");


// for (let [indice, caja] of cajas.entries()) {
//     caja.addEventListener("click", function (e) {
//         e.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
//         e.target.innerHTML = "";
//     });


// }

// realizarlo con for

// for (let i = 0; i < cajas.length; i++) {
//     cajas[i].addEventListener('click', function (evento) {
//         evento.target.style.backgroundImage = "url(img/" + (i + 1) + ".jpg)";
//         evento.target.innerHTML = "";
//     });
// }

// con un foreach

cajas.forEach(function (valor, indice) {
    valor.addEventListener('click', function (evento) {
        evento.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
        evento.target.innerHTML = "";
    });
});