const cajas = document.querySelectorAll("div");

/*cajas.forEach(function(caja, indice) {
    caja.addEventListener("click", function(e) {
        if (e.target.style.backgroundImage == "") {
            e.target.style.backgroundImage = "url('img/" + (indice + 1) + ".jpg')";
            e.target.innerHTML = "";
        } else {
            e.target.style.backgroundColor = "red";
            e.target.style.backgroundImage = "";
        }

    });
});*/

// Array para comprobar que tengo pulsado
const pulsados = [0, 0, 0, 0, 0];

cajas.forEach(function (caja, indice) {
    caja.addEventListener("click", function (e) {
        if (pulsados[indice] == 1) {
            e.target.style.backgroundColor = "red";
            e.target.style.backgroundImage = "";
            pulsados[indice] = 0;
        } else {
            e.target.style.backgroundImage = "url('img/" + (indice + 1) + ".jpg')";
            e.target.innerHTML = "";
            pulsados[indice] = 1;
        }

    });
});