// variables y constantes

const li = document.querySelectorAll('ul>li');


// recorrer el Nodelist y colocar onclick
// cuando pulsemos sobre el li nos debe mostrar el dia de la semana pulsado en un alert

li.forEach(function (valor) {
    valor.addEventListener('click', function (evento) {
        let diaSemana = evento.target.innerHTML;
        alert(diaSemana);
    });
});
