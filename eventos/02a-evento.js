const cajas = document.querySelectorAll("div");
// PRECARGA DE IMAGENES para que las guarde en la memoria RAM y no tarde tanto tiempo en cargarlas
// creo un array de imagenes
const imagenes = [
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image(),
];
//asigno acada imagen su src
imagenes[0].src = "img/1.jpg";
imagenes[1].src = "img/2.jpg";
imagenes[2].src = "img/3.jpg";
imagenes[3].src = "img/4.jpg";
imagenes[4].src = "img/5.jpg";


for (let c = 0; c < cajas.length; c++) {
    cajas[c].addEventListener("click", function (e) {
        e.target.style.backgroundImage = "url( ' " + imagenes[c].src + " ')";
        e.target.innerHTML = "";
    });
}


/*for (let [indice, caja] of cajas.entries()) {
    caja.addEventListener("click", function(e) {
        e.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
        e.target.innerHTML = "";
    });

}*/