document.querySelector('button')
    .addEventListener("click",
        function(evento) {
            // objeto event
            console.log(evento);

            // element sobre el que se ha producido el evento click
            console.log(evento.target);

            //element que esta escuchando (boton)
            console.log(this);
        });