/**
 * recibe un objeto de tipo
 * {
 * color: "red",
 * radio: 40,
 * borde: 2,
 * colorBorde: "black"
 * x: 15,
 * y:20
 * }
 */
function circulo(datos) {

    let div = document.createElement("div");

    div.style.left = datos.x + "px";
    div.style.top = datos.y + "px";
    div.style.width = datos.radio * 2 + "px";
    div.style.height = datos.radio * 2 + "px";
    div.style.backgroundColor = datos.color;
    div.style.borderRadius = datos.radio * 2 + "px";
    div.style.position = "absolute";
    div.style.borderWidth = datos.borde + "px";
    div.style.borderColor = datos.colorBorde;
    div.style.borderStyle = "solid";

    document.body.appendChild(div);

}

circulo({
    color: "red",
    x: 15,
    y: 20,
    colorBorde: "black",
    radio: 40,
    borde: 2,
})

circulo({
    color: "red",
    x: 150,
    y: 200,
    colorBorde: "black",
    radio: 40,
    borde: 2,
})