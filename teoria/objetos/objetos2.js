/*
    objeto con los siguientes miembros
    objeto loro
        MIEMBROS
            propiedades
                color
                peso
            metodos
                hablar
                volar
*/

// NOTACION JSON
let loro = {
    color: "azul",
    peso: 10,
    hablar: function() {
        return "piopio";
    },
    volar: function() {
        return "volando voy";
    }
};


// cuidado
let loro1 = loro; // estamos pasando el loro por referencia

/*
lo que cambie en loro afecta a loro1 y viceversa
*/

loro.plumas = true;

console.log(loro1.plumas);