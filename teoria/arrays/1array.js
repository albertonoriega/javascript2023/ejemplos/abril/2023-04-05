let a = [1, 2, 3, 4, 12, 34];

for (let valor of a) {
    // valor
    console.log(valor);
}

for (let [indice, valor] of a.entries()) {
    //indice
    console.log(indice);

    //valor
    console.log(valor);
}

a.forEach(function(valor, indice) {
    // valor
    console.log(valor);

    //indice
    console.log(indice);
});