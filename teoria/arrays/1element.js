//NodeList
let li = document.querySelectorAll('ul>li');

console.log("Mostrando con un for");
for (let i = 0; i < li.length; i++) {
    console.log(li[i]);
}

console.log("Mostrando con for of");
for (const valor of li) {
    console.log(valor.innerHTML);
}

console.log("Mostrando con foreach");
li.forEach(function (valor) {
    console.log(valor.innerHTML);
});

console.log("Mostrando con un for in");
for (let indice in li) {
    console.log(li[indice].innerHTML);
}